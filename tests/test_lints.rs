use displaythis::Display;

pub use std::error::Error;

#[test]
fn test_unused_qualifications() {
    #![deny(unused_qualifications)]

    // Expansion of derive(Error) macro can't know whether something like
    // std::error::Error is already imported in the caller's scope so it must
    // suppress unused_qualifications.

    #[derive(Debug, Display)]
    #[display("...")]
    pub struct MyError;

    let _: MyError;
}
