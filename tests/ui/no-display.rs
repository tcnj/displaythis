use displaythis::Display;

#[derive(Debug)]
struct NoDisplay;

#[derive(Display, Debug)]
#[display("thread: {thread}")]
pub struct Error {
    thread: NoDisplay,
}

fn main() {}
