use displaythis::Display;

#[derive(Display)]
pub union U {
    msg: &'static str,
    num: usize,
}

fn main() {}
