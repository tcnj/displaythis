use displaythis::Display;

#[derive(Display, Debug)]
#[display("...")]
#[display("...")]
pub struct Error;

fn main() {}
