#![deny(clippy::all, clippy::pedantic)]
#![cfg(feature = "std")]

use displaythis::Display;
use ref_cast::RefCast;
use std::fmt::Display;
use std::path::{Path, PathBuf};

#[derive(Display, Debug)]
#[display("failed to read '{file}'")]
struct StructPathBuf {
    file: PathBuf,
}

#[derive(Display, Debug, RefCast)]
#[repr(C)]
#[display("failed to read '{file}'")]
struct StructPath {
    file: Path,
}

#[derive(Display, Debug)]
enum EnumPathBuf {
    #[display("failed to read '{0}'")]
    Read(PathBuf),
}

fn assert<T: Display>(expected: &str, value: T) {
    assert_eq!(expected, value.to_string());
}

#[test]
fn test_display() {
    let path = Path::new("/displaythis");
    let file = path.to_owned();
    assert("failed to read '/displaythis'", StructPathBuf { file });
    let file = path.to_owned();
    assert("failed to read '/displaythis'", EnumPathBuf::Read(file));
    assert("failed to read '/displaythis'", StructPath::ref_cast(path));
}
